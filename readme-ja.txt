=== Plugin Name ===
Contributors: Webnist, megumithemes
Donate link: 
Tags: Private, Maintenance
Requires at least: 3.4
Tested up to: 3.4
Stable tag: 0.1

プラーベート用やメンテナンス用

== Description ==
このプラグインを有効化するとログインしなければ閲覧することが出来ないため、
プライベートなブログを開始できます。
また、知り合い同士のコミュニケーションの為に使用することもできます。

あなたが、制作者の場合デモでサイトを作った場合BASIC認証をかける場合がありますが、それはスマートではありません。
このプラグインを使用すれば煩わしい、BASIC認証ともさよならできます。

プラグインの由来について。
日本には”Ichigen-San Okotowari”という文化があります。
"Ichigen-San Okotowari"言えば、お店に全く関わりのない初めての人は入店を断られることを意味する。そのような店に入るためには、その店に面識のある人物からなんらかの紹介をされる必要がある。

== Installation ==
1. `ichigen-san` フォルダーを `/wp-content/plugins/` アップロード。
2. プラグインメニューより有効化。

== Upgrade Notice ==

Version 0.1.7 メンテナンスページの追加

== Changelog ==

= 0.1.7 =
* メンテナンスページの追加

= 0.1 =
* 最初のバージョン