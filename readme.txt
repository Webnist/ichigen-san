=== Plugin Name ===
Contributors: Webnist, megumithemes
Donate link: 
Tags: Private, Maintenance
Requires at least: 3.4
Tested up to: 3.4
Stable tag: 0.1

Private publishing and maintenance

== Description ==
Because you can not be viewed unless you log When you activate this plug-in,
You can start a private blog.
Furthermore, you can also be used for communication between the acquaintance.

You, there is a case to put the BASIC authentication if you create a site with demonstration case of the author, it is not smart.
Troublesome if you use this plugin, you can also authenticate goodbye BASIC.

About the origin of the plug-in.
There is a culture of "Ichigen-San Okotowari" in Japan.
Speaking of "Ichigen-San Okotowari", the first person not related at all to the shop is meant to be refused to enter the shop. To enter into such stores must be some referrals from persons acquainted with the shop.

== Installation ==
1. Upload the entire `ichigen-san` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.

== Upgrade Notice ==

Version 0.1.7 Additional maintenance page

== Changelog ==

= 0.1.7 =
* Additional maintenance page

= 0.1 =
* First version